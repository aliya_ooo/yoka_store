import Vue from 'vue'
import Router from 'vue-router'
import AppHeader from '@/components/AppHeader'
import AppDelivery from '@/components/AppDelivery'
import Home from '@/pages/Home'
import StoreProducts from '@/pages/StoreProducts'
import AppNewcollection from '@/components/AppNewcollection'
import ProductList from '@/components/product/ProductList'
import Main from '@/pages/Admin/Main'
import New from '@/pages/Admin/New'
import Details from '@/pages/Details'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      children: [
        {
          path: '/app-header',
          name: 'app-header',
          component: AppHeader
        },
        {
          path: '/app-delivery',
          name: 'app-delivery',
          component: AppDelivery
        },
        {
          path: '/app-newcollection',
          name: 'app-newcollection',
          component: AppNewcollection
        }
      ]
    },
    {
      path: '/catalog/:category_id',
      name: 'catalog',
      component: StoreProducts,
      children: [
        {
          path: '/product-list/:category_id',
          name: 'product-list',
          component: ProductList
        }
      ]
    },
    // {
    //   path: '/customer-cart',
    //   name: 'customer-cart',
    //   component: CustomerCart
    // },
    {
      path: '/main',
      name: 'main',
      component: Main,
      children: [
        {
          path: '/new',
          name: 'new',
          component: New
        }
      ]
    },
    {
      path: '/details/:id',
      name: 'details',
      props: true,
      component: Details
    }
  ]
})
