export default {
  state: {
    cart: [
      {
        name: 'T-Shirt',
        price: '123'
      }
    ],
    cartCount: 0,
    product: {},
    products: [],
    category: [
      {
        name: "Платья",
        id: 1
      },
      {
        name: "Блузки",
        id: 2
      },
      {
        name: "Рубашки",
        id: 3
      },
      {
        name: "Топы",
        id: 4
      },
      {
        name: "Пиджаки",
        id: 5
      },
      {
        name: "Брюки",
        id: 6
      },
      {
        name: "Джинсы",
        id: 7
      }
    ],
    colors: [
      {
        name: "Черные",
        id: 1
      },
      {
        name: "Белый",
        id: 2
      },
      {
        name: "Бежевый",
        id: 3
      },
      {
        name: "Коричневый",
        id: 4
      },
      {
        name: "Красный",
        id: 5
      },
      {
        name: "Желтый",
        id: 6
      }
    ],
    sizes: [
      {
        name: "Стандартный",
        id: 1
      },
      {
        name: "XS",
        id: 2
      },
      {
        name: "S",
        id: 3
      },
      {
        name: "M",
        id: 4
      },
      {
        name: "L",
        id: 5
      },
      {
        name: "XL",
        id: 6
      }
    ]
  },
  getters: {
    products (state) {
      return state.products
    },
    productById (state) {
      return productId => {
        return state.products.find(product => product.id === productId)
      }
    }
  },
  actions: {
    loadProducts ({ commit }, category_id=0) {
      this.axios
        .get(`http://localhost:3000/clothes?category=${category_id}`)
        .then(r => r.data)
        .then(products => {
          commit('SET_PRODUCTS', products)
        })
    }
  }
}