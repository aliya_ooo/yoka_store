import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import VueAxios from "vue-axios";
import products from './modules/products'

Vue.use(Vuex);
Vue.use(VueAxios, axios);

export default new Vuex.Store({
  modules: {
    products
  },
  state: {
    cart: [],
    cartCount: 0,
    newProduct: {},
    products: [],
    // categories: [],
    categories: [
      {
        name: "Платья",
        id: 1
      },
      {
        name: "Блузки",
        id: 2
      },
      {
        name: "Рубашки",
        id: 3
      },
      {
        name: "Топы",
        id: 4
      },
      {
        name: "Пиджаки",
        id: 5
      },
      {
        name: "Брюки",
        id: 6
      },
      {
        name: "Джинсы",
        id: 7
      }
    ],
    colors: [
      {
        name: "Черные",
        id: 1
      },
      {
        name: "Белый",
        id: 2
      },
      {
        name: "Бежевый",
        id: 3
      },
      {
        name: "Коричневый",
        id: 4
      },
      {
        name: "Красный",
        id: 5
      },
      {
        name: "Желтый",
        id: 6
      }
    ],
    sizes: [
      {
        name: "Стандартный",
        id: 1
      },
      {
        name: "XS",
        id: 2
      },
      {
        name: "S",
        id: 3
      },
      {
        name: "M",
        id: 4
      },
      {
        name: "L",
        id: 5
      },
      {
        name: "XL",
        id: 6
      }
    ]
  },
  actions: {
    loadProducts ({ commit }, category_id=0) {
      axios
        .get(`http://localhost:3000/clothes?category=${category_id}`)
        .then(r => r.data)
        .then(products => {
          commit('SET_PRODUCTS', products)
        })
    },
    // loadCategories ({ commit }) {
    //   axios
    //     .get(`http://localhost:3000/categories`)
    //     .then(c => c.data)
    //     .then(categories => {
    //       commit('SET_CATEGORY', categories)
    //     })
    // },
    addProductToCart (context, product) {
      if (product.inventory > 0) {
        const cartItem = context.state.cart.find(item => item.id === product.id)
        if (!cartItem) {
          context.commit('PUSH_PRODUCT_TO_CART', product.id)
        } else {
          alert("There is no such product left")
        }
      }
    }
  },
  mutations: {
    SET_PRODUCTS (state, products) {
      state.products = products
    },
    SET_CATEGORY (state, categories) {
      state.categories = categories
    },
    addToCart(state, product) {
      state.cart.push(product);

      state.cartCount++;
      console.log(product.name);
    }
    // addToCart(state, product) {
    //   let found = state.cart.find(p => p.id === product.id);
  
    //   if (found) {
    //       found.q ++;
    //       found.totalPrice = found.q * found.price;
    //   } else {
    //       state.cart.push(product);
  
    //       Vue.set(product, 'q', 1);
    //       Vue.set(product, 'totalPrice', product.price);
    //   }
  
    //   state.cartCount++;
    // },
  //   removeFromCart(state, product) {
  //     let index = state.cart.indexOf(product);
  
  //     if (index > -1) {
  //         let product = state.cart[index];
  //         state.cartCount -= product.q;
  
  //         state.cart.splice(index, 1);
  //     }
  // }
    // PUSH_PRODUCT_TO_CART (state, itemId) {
    //   state.cart.push({
    //     id: itemId
    //   })
    // }
  },
  getters: {
    productById (state) {
      return productId => {
        return state.products.find(product => product.id === productId)
      }
    }
  }
})
